﻿using System;
namespace SeaBattleNewVersion
{
    public class Cell
    {
        int _coordX;

       internal int coordX
        {
            get { return _coordX; }
            set { _coordX = value; }
        }

        int _coordY;

       internal int coordY
        {
            get { return _coordY; }
            set { _coordY = value; }
        }

        bool _hit;

        internal bool hit
        {
            get { return _hit; }
            set { _hit = value; }
        }

        bool _occupied;

        internal bool occupied
        {
            get { return _occupied; }
            set { _occupied = value; }
        }

        public Cell()
        {
        }

    }
}
