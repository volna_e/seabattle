﻿using System;
namespace SeaBattleNewVersion
{
    public class UserPlayer : Player
    {
        Cell cell;

        protected override void setShips(PlayGround playGround)
        {
            Console.WriteLine("Установка Ваших кораблей началась \n");

            //одноклеточные корабли
            Ship shipSizeOneFirst = new Ship();
            shipSizeOneFirst.addShipSizeOneCell(playGround, 4, 1);

            Ship shipSizeOneSecond = new Ship();
            shipSizeOneSecond.addShipSizeOneCell(playGround, 10, 1);

            Ship shipSizeOneThird = new Ship();
            shipSizeOneThird.addShipSizeOneCell(playGround, 8, 2);

            Ship shipSizeOneFourth = new Ship();
            shipSizeOneFourth.addShipSizeOneCell(playGround, 1, 10);

            //двухклеточные корабли
            Ship shipSizeTwoFirst = new Ship();
            shipSizeTwoFirst.addShipSizeTwoCells(playGround, 6, 2, 6, 3);

            Ship shipSizeTwoSecond = new Ship();
            shipSizeTwoSecond.addShipSizeTwoCells(playGround, 3, 10, 4, 10);

            Ship shipSizeTwoThird = new Ship();
            shipSizeTwoThird.addShipSizeTwoCells(playGround, 8, 5, 9, 5);

            //трехклеточные корабли
            Ship shipSizeThreeFirst = new Ship();
            shipSizeThreeFirst.addShipSizeThreeCells(playGround, 2, 7, 3, 7, 4, 7);

            Ship shipSizeThreeSecond = new Ship();
            shipSizeThreeSecond.addShipSizeThreeCells(playGround, 7, 7, 7, 8, 7, 9);

            //четырехклеточные корабли
            Ship shipSizeFour = new Ship();
            shipSizeFour.addShipSizeFourCells(playGround, 2, 2, 2, 3, 2, 4, 2, 5);


        }

        protected override Cell GetCoordinates(PlayGround playGround)
        {
            cell = new Cell();

            Console.WriteLine("Ваш ход ");
            cell.coordX = readline('x');
            cell.coordY = readline('y');

            return cell;
        }


        int readline(char charapter)
        {
            int rl;
            do
            {
                Console.WriteLine($"Введите координату {charapter} : [1-10]");

                try
                {
                    rl = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Недопустимое значение. Повторите ввод ");
                    continue;
                }

                if (rl > 10 || rl < 1)
                    Console.WriteLine("Значение превышает норму. Повторите ввод ");
                else break;
            }

            while (true);

            return rl;

        }

        public UserPlayer()
        {
        }
    }
}
