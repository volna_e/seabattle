﻿using System;
namespace SeaBattleNewVersion
{
    public class Game
    {
        Player player;
        Player opponent;

        bool playerWin;
        bool opponentWin;

        public Game()
        {
            player = new UserPlayer();
            opponent = new CpuPlayer();
        }

        public void NextGame()
        {
            player.SetEnemy(opponent);
            opponent.SetEnemy(player);

            player.Start();
            opponent.Start();

            if (Ship.cheakCountShips())
            {
                Console.WriteLine("Установка кораблей успешно завершена. Игра началась! \n");

                while (!playerWin && !opponentWin)
                {
                    player.MakeMove(out playerWin);

                    if (!playerWin)
                        opponent.MakeMove(out opponentWin);
                }

                if (playerWin)
                {
                    Console.WriteLine("Вы победили!");
                }

                if (opponentWin)
                {
                    Console.WriteLine("Компьютер победил!");
                }

                Console.WriteLine("Конец игры");
            }


        }
    }
}
