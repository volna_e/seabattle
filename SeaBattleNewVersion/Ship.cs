﻿using System;
namespace SeaBattleNewVersion
{
    public class Ship
    {
        const int maxSize = 4;
        Cell[] coord = new Cell[maxSize];

        static int countShipSizeOneCellFree = 8;
        static int countShipSizeTwoCellsFree = 6;
        static int countShipSizeThreeCellsFree = 4;
        static int countShipSizeFourCellsFree = 2;

        public Ship()
        {
            for (int i = 0; i < maxSize; i++)
            {
                coord[i] = new Cell();
            }
        }


        bool createShipSizeOneCell(PlayGround playGround, int x, int y)
        {

            return coordAssignToCell(playGround, 1, x, y);

        }

        bool createShipSizeTwoCells(PlayGround playGround, int x1, int y1, int x2, int y2)
        {

            return createShipSizeOneCell(playGround, x1, y1) && coordAssignToCell(playGround, 2, x2, y2);
        }

        bool createShipSizeThreeCells(PlayGround playGround, int x1, int y1, int x2, int y2, int x3, int y3)
        {

            return createShipSizeTwoCells(playGround, x1, y1, x2, y2) && coordAssignToCell(playGround, 3, x3, y3);

        }

        bool createShipSizeFourCells(PlayGround playGround, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
        {

            return createShipSizeThreeCells(playGround, x1, y1, x2, y2, x3, y3) && coordAssignToCell(playGround, 4, x4, y4);

        }

        public void addShipSizeOneCell(PlayGround playGround, int x, int y)
        {
            if (createShipSizeOneCell(playGround, x, y))
            {
                countShipSizeOneCellFree--;
                playGround.onDamage += isAttacked;
                Console.WriteLine("Одноклеточный корабль добавлен");
            }

            else
            {
                Console.WriteLine("Одноклеточный корабль не был построен");
            }
        }

        public void addShipSizeTwoCells(PlayGround playGround, int x1, int y1, int x2, int y2)
        {
            if (createShipSizeTwoCells(playGround, x1, y1, x2, y2))
            {
                countShipSizeTwoCellsFree--;
                playGround.onDamage += isAttacked;
                Console.WriteLine("Двухклеточный корабль добавлен");
            }

            else
            {
                Console.WriteLine("Двухклеточный корабль не был построен");
            }
        }

        public void addShipSizeThreeCells(PlayGround playGround, int x1, int y1, int x2, int y2, int x3, int y3)
        {
            if (createShipSizeThreeCells(playGround, x1, y1, x2, y2, x3, y3))
            {
                countShipSizeThreeCellsFree--;
                playGround.onDamage += isAttacked;
                Console.WriteLine("Трехклеточный корабль добавлен");
            }

            else
            {
                Console.WriteLine("Трехклеточный корабль не был построен");
            }
        }

        public void addShipSizeFourCells(PlayGround playGround, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
        {
            if (createShipSizeFourCells(playGround, x1, y1, x2, y2, x3, y3, x4, y4))
            {
                countShipSizeFourCellsFree--;
                playGround.onDamage += isAttacked;
                Console.WriteLine("Четырехклеточный корабль добавлен");
            }

            else
            {
                Console.WriteLine("Четырехклеточный корабль не был построен");
            }
        }

        public static bool cheakCountShips()
        {
            if (countShipSizeOneCellFree != 0 || countShipSizeTwoCellsFree != 0 || countShipSizeThreeCellsFree != 0 || countShipSizeFourCellsFree != 0)
            {
                Console.WriteLine("Число кораблей на игровом поле не укладывается в норму. Перераспределите корабли и повторите попытку");
                return false;
            }
            else return true;

        }


        bool coordAssignToCell(PlayGround playground, int number, int x, int y)
        {
            var isNear = false;

            if (x < 0 || x > 10 || y < 0 || y > 10)
            {
                Console.WriteLine($"Ошибка расстановки! Указанное значение превышает норму: {x} , {y} ");
                return false;
            }

            if (playground.isOccupied(x, y))
            {
                Console.WriteLine($"Ошибка расстановки! Повторяющиеся координаты: {x} , {y} ");
                return false;
            }

            else if (playground.checkRuleOfNearCells(this, x, y, out isNear) && pointIsAlong(x, y))
            {
                if (!isNear && !isNull())
                {
                    Console.WriteLine($"Ошибка расстановки! Неверно указана позиция: {x} , {y} ");
                    return false;
                }

                else
                {
                    coord[number - 1].coordX = x;
                    coord[number - 1].coordY = y;
                    coord[number - 1].occupied = true;

                    playground.occup(x, y);
                    return true;
                }

            }

            return false;
        }

        bool pointIsAlong(int x, int y)
        {

            if (coord[0].occupied && coord[1].occupied)
            {
                if (coord[0].coordX == coord[1].coordX && coord[1].coordX == x)
                    return true;

                else if (coord[0].coordY == coord[1].coordY && coord[1].coordY == y)
                    return true;

                else
                {
                    Console.WriteLine($"Ошибка расстановки! Неверно указана позиция: {x} , {y}");
                    return false;
                }
            }

            return true;

        }

        bool isAttacked(int x, int y)
        {
            if (!isNull())

                foreach (Cell cell in coord)
                    if (cell.coordX == x && cell.coordY == y)
                    {
                        cell.occupied = false;
                        cell.hit = true;
                        Console.WriteLine("Попадание в корабль \n");

                        if (isNull())
                            Console.WriteLine("Корабль потоплен \n");

                        return true;

                    }

            return false;
        }

        internal bool isOwnShip(int x, int y)
        {

            var isOwnShip = false;

            foreach (Cell cell in coord)
                if (cell.coordX == x && cell.coordY == y)
                    isOwnShip = true;

            return isOwnShip;

        }

        bool isNull()
        {

            foreach (Cell cell in coord)
                if (cell.occupied)
                    return false;

            return true;
        }


    }
}
