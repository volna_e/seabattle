﻿using System;
namespace SeaBattleNewVersion
{
    public abstract class Player
    {
        
        readonly PlayGround playground = new PlayGround();
        Player Enemy;

        protected abstract Cell GetCoordinates(PlayGround playGround);

        protected abstract void setShips(PlayGround playGround);

        public void Start()
        {
            setShips(playground);
        }

        public void MakeMove(out bool gameOver)
        {
            gameOver = false;
            var coords = GetCoordinates(Enemy.playground);

            if (attack(coords.coordX, coords.coordY))
                if (!notShipsEnemys())
                    MakeMove(out gameOver);
                else gameOver = true;
        }

        public Player()
        {
        }

        public void SetEnemy(Player enemy)
        {
            Enemy = enemy;
        }

        bool attack(int x, int y)
        {
            return Enemy.playground.damage(x, y);
        }

        bool notShipsEnemys()
        {
            return Enemy.playground.playGroundIsEmpty();
        }
    }
}



