﻿using System;
namespace SeaBattleNewVersion
{
    public class PlayGround
    {
        internal delegate bool damageHandler(int x, int y);
        internal event damageHandler onDamage;

        const int sizePlayGround = 10;

        Cell[,] cells = new Cell[sizePlayGround, sizePlayGround];

        public PlayGround()
        {
            for (int i = 0; i < sizePlayGround; i++)
                for (int k = 0; k < sizePlayGround; k++)
                    cells[i, k] = new Cell();
        }

        internal bool damage(int x, int y)
        {

            if (cells[x - 1, y - 1].occupied)
            {
                cells[x - 1, y - 1].occupied = false;
                cells[x - 1, y - 1].hit = true;
                onDamage?.Invoke(x, y);
                return true;
            }

            else
            {
                cells[x - 1, y - 1].hit = true;
                Console.WriteLine("Мимо \n");
                return false;
            }
        }

        internal bool playGroundIsEmpty()
        {
            foreach (Cell cell in cells)
                if (cell.occupied)
                    return false;

            return true;
        }

        internal bool isHit(int x, int y)
        {
            if (cells[x - 1, y - 1].hit)
            {
                return true;
            }

            return false;
        }

        internal bool isOccupied(int x, int y)
        {
            if (cells[x - 1, y - 1].occupied)
            {
                return true;
            }

            return false;
        }

        internal void occup(int x, int y)
        {
            cells[x - 1, y - 1].occupied = true;
        }

        internal bool checkRuleOfNearCells(Ship ship, int x, int y, out bool isNearOwnShip)
        {
            isNearOwnShip = false;
            var isNearAnotherShip = false;

            for (int diffX = -1; diffX < 2; diffX++)
            {
                for (int diffY = -1; diffY < 2; diffY++)
                {
                    try
                    {
                        if (cells[x - 1 + diffX, y - 1 + diffY].occupied)
                        {
                            if (!ship.isOwnShip(x + diffX, y + diffY))
                            {
                                Console.WriteLine($"Ошибка расстановки! Корабли расположены слишком близко друг к другу. Координаты: {x} , {y} ");
                                isNearAnotherShip = true;
                            }
                            else
                                isNearOwnShip = true;
                        }

                    }

                    catch { }

                }
            }

            return isNearAnotherShip ? false : true;

        }



    }
}
