﻿using System;
namespace SeaBattleNewVersion
{
    public class CpuPlayer : Player
    {
        Cell cell = new Cell();

        const int side = 10;
        Random random = new Random();


        protected override void setShips (PlayGround playGround)
        {
            Console.WriteLine("Установка кораблей соперника началась \n");

            //одноклеточные корабли
            Ship shipSizeOneFirst = new Ship();
            shipSizeOneFirst.addShipSizeOneCell(playGround, 1, 2);

            Ship shipSizeOneSecond = new Ship();
            shipSizeOneSecond.addShipSizeOneCell(playGround, 1, 10);

            Ship shipSizeOneThird = new Ship();
            shipSizeOneThird.addShipSizeOneCell(playGround, 10, 1);

            Ship shipSizeOneFourth = new Ship();
            shipSizeOneFourth.addShipSizeOneCell(playGround, 6, 7);

            //двухклеточные корабли
            Ship shipSizeTwoFirst = new Ship();
            shipSizeTwoFirst.addShipSizeTwoCells(playGround, 6, 2, 6, 3);

            Ship shipSizeTwoSecond = new Ship();
            shipSizeTwoSecond.addShipSizeTwoCells(playGround, 1, 8, 2, 8);

            Ship shipSizeTwoThird = new Ship();
            shipSizeTwoThird.addShipSizeTwoCells(playGround, 7, 5, 8, 5);

            //трехклеточные корабли
            Ship shipSizeThreeFirst = new Ship();
            shipSizeThreeFirst.addShipSizeThreeCells(playGround, 3, 3, 3, 4, 3, 5);

            Ship shipSizeThreeSecond = new Ship();
            shipSizeThreeSecond.addShipSizeThreeCells(playGround, 4, 8, 4, 9, 4, 10);

            //четырехклеточные корабли
            Ship shipSizeFour = new Ship();
            shipSizeFour.addShipSizeFourCells(playGround, 6, 9, 7, 9, 8, 9, 9, 9);

        }

        protected override Cell GetCoordinates(PlayGround playGround)
        {
            int randomX;
            int randomY;

            do
            {
                randomX = random.Next(1, side);
                randomY = random.Next(1, side);

            }
            while (playGround.isHit(randomX, randomY));

            cell.coordX = randomX;
            cell.coordY = randomY;

            Console.WriteLine($"Ход соперника: {cell.coordX} , {cell.coordY} ");

            return cell;
        }


        public CpuPlayer()  
        {
        
        }
    }

}
    

